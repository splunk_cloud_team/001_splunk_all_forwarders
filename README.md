# 001_splunk_all_forwarders

This app defines the base configuration common for all forwarders (Universal and Heavy) in the environment.

### Why do I need this? ###

These settings provide the foundation for optimal data distribution in SplunkCloud.  
Using the default Splunk settings often results in poor data distribution leading to blocked queues, latency, and poor search performance.  

Additionally, this disables the forwarding filter, which allows for full diagnosis of any issues from the \_internal SplunkCloud index.

### Setting provided ###

* outputs.conf
	* [tcpout]
	* autoLBFrequency = 5
	* autoLBVolume = 1048576
	* forwardedindex.filter.disable = true

### How do I get set up? ###

* Add to the build script of any machine not configured using the Deployment Server
	* place the app in the $SPLUNK_HOME/etc/apps folder
	* OR merge these settings into the same files in $SPLUNK_HOME/etc/system/local
	
* Add to the $SPLUNK_HOME/etc/deployment-apps folder on the Deployment Server
	* Create or modify the serverclass that matches "include=*"

### Why is this app named funny? ###

Due to the way Splunk assigns precedence to apps and files, numbered files are applied first,  
followed by UPPERCASE letters, then finally lowercase letters. (ASCII sort order). 

The first setting "wins" when set multiple times across apps. Naming the app in this fashion  
assures that these settings are not overridden by another app later in the sort order.  

The exception to this is $SPLUNK_HOME/etc/system/local which always wins.

### Who do I talk to? ###

Michael F. Doyle, SplunkCloud PS  
mdoyle@splunk.com